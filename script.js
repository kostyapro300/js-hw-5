// 1:  Опишіть своїми словами, що таке метод об'єкту
// В:  функція, яка призначена для виконання певних дій або операцій з об'єктом. 

// 2: Який тип даних може мати значення властивості об'єкта?
// В: Будь які. Рядки, числа, массиви, булеанс, функции, символи и т.д

// 3:  Об'єкт це посилальний тип даних. Що означає це поняття?
// В: Це означає, що змінні які містять об'єкти, фактично зберігають не сам об'єкт
// а посилання на об'єкт у пам'яті комп'ютера.







function createNewUser() {
    const firstName = prompt('Введіть ваше ім\'я');
    const lastName = prompt('Введіть ваше прізвище');

    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function() {
            return firstName[0] + lastName
        }
    };

    return newUser;
}

const user = createNewUser();
console.log(user);

const login = user.getLogin()
console.log(login)